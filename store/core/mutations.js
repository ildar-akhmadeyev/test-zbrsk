export default {
	setIsMenuOpen: state => (state.isMenuOpen = !state.isMenuOpen),
	changePrize: state => (state.isReservedSelected = !state.isReservedSelected)
};